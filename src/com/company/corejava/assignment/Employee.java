package com.company.corejava.assignment;

public class Employee extends Person {
    private long employeeId;
    private String department;

    public Employee(String name) {
        super(name);
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
