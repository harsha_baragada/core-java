package com.company.corejava.assignment;

public class Customer extends Person {

    private long accountNumber;
    private String address;
    private double balance;
    private boolean isAccountActive;

    public Customer(String name) {
        super(name);
        System.out.println(name);
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isAccountActive() {
        return isAccountActive;
    }

    public void setAccountActive(boolean accountActive) {
        isAccountActive = accountActive;
    }


    public String withDrawMoney(double balance, double withdwal) {
        String response = "failure";
        if (balance > withdwal) {
            response = "success";
        }
        return response;
    }

    public double depositMoney(double balance, double depositAmount) {
        return balance + depositAmount;
    }
}
