package com.company.corejava;

public class User {

    private int id;
    private String name;
    private String email;
    private  long mobileNumber;
    private String city;
    private int zip;

    public User(int id, String name, String email, long mobileNumber, String city, int zip) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.city = city;
        this.zip = zip;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobileNumber=" + mobileNumber +
                ", city='" + city + '\'' +
                ", zip=" + zip +
                '}';
    }
}
