package com.company.corejava;

public class ExampleLoopControls {


    //Break statement
    public void exampleBreakStatement(String[] fruits) {

        for (int i = 0; i < fruits.length; i++) {
            if (i == 6) {
                break;
            }
            System.out.println(fruits[i]);
            System.out.println("index " + i);

        }
    }


    //continue statement
    public void exampleContinueStatement(String[] fruits) {
        System.out.println("example break statement");

        for (int i = 0; i < fruits.length; i++) {
            if (i == 3) {
                System.out.println("The statement is " + (i == 3) + " and index is " + i);
                continue;
            }
            System.out.println(fruits[i]);
            System.out.println("index " + i);

        }
    }

}
