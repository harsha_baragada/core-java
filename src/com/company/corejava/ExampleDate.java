package com.company.corejava;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExampleDate {


    public void printDateResults() throws InterruptedException {
        Date date = new Date();
        System.out.println(date);
        Date later = new Date();

        Thread.sleep(10*60*1000);
        System.out.println(later.getTime());


        System.out.println(date.after(later));
        System.out.println(date.before(later));
        System.out.println(date.equals(later));
        long mil = 1602639634063L;
        date.setTime(mil);
        System.out.println(date.toString());


    }

    public void getFormattedDate() throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println("raw date "+date);
        Date formattedDate = format.parse("1994-08-12");
        System.out.println("formatted date "+formattedDate);



    }
}
