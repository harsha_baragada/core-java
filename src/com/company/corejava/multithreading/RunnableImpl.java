package com.company.corejava.multithreading;

public class RunnableImpl implements Runnable {

    private Thread t;
    private String name;

    public RunnableImpl(String name) {
        this.name = name;
        System.out.println("Creating thread " + name);
    }

    @Override
    public void run() {

        System.out.println("The thread " + name);


        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Thread " + name + " " + i);

                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread " + name + " exiting");
    }

    public void start() {
        System.out.println(name + " thread is starting");
        if (t == null) {
            t = new Thread(this::run, name);
            t.start();
        }
    }

}
