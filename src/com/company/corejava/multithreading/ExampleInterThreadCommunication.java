package com.company.corejava.multithreading;

class Chat {

    boolean flag = false;

    public synchronized void Question(String msg) {
        if (flag) {

            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        System.out.println(msg);
        flag = true;
        notify();
    }

    public synchronized void Answer(String msg) {
        if (!flag) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(msg);
        flag = false;
        notify();
    }
}


class T1 implements Runnable {
    Chat chat;
    String[] s1 = {"Hi", "How are you.?", "I am also doing fine"};

    public T1(Chat chat) {
        this.chat = chat;
        new Thread(this, "Question").start();
    }


    @Override
    public void run() {
        for (int i = 0; i < s1.length; i++) {
            chat.Question(s1[i]);
        }
    }
}

class T2 implements Runnable {
    Chat chat;
    String[] s1 = {"Hi", "I am good", "What about you .?"};


    public T2(Chat chat) {
        this.chat = chat;
        new Thread(this::run, "Answer").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < s1.length; i++) {
            chat.Answer(s1[i]);
        }
    }
}

public class ExampleInterThreadCommunication {

    public static void main(String[] args) {
        Chat m = new Chat();
        new T1(m);
        new T2(m);
    }
}