package com.company.corejava.inheritence;

public class Deer extends Mammal{

    public Deer(String category) {
        super(category);
    }

    public String whoAmI() {
        return "I am a deer";
    }
}
