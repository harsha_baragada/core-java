package com.company.corejava.inheritence;

public class ExampleChildClass extends ExampleParentClass {

    public void printAStatementInChild() {
        System.out.println("Just a sample statement from child");
    }

    public void printAStatement(){
        System.out.println("I am from the first child class");
    }
}
