package com.company.corejava.inheritence;

public class Mammal extends Animal {
    public Mammal(String category) {
        super(category);

    }

    public String whoAmI() {
        return "I am an Mammal";
    }
}
