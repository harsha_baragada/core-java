package com.company.corejava.inheritence;

public class Animal {
    String category;

    public Animal(String category) {
        this.category = category;
        System.out.println(category);
    }

    public Animal() {
        System.out.println("Animal class has been invoked");
    }

    public String whoAmI() {
        return "I am an animal";
    }
}
