package com.company.corejava.operators;

public class ExampleLogicalOperators {

    public boolean logicalAnd(boolean a, boolean b) {
        return a && b;
    }


    public boolean logicalOr(boolean a, boolean b) {
        return a || b;
    }

    public boolean logicalAndNot(boolean a, boolean b) {
        return !(a && b);
    }

    public boolean logicalOrNot(boolean a, boolean b) {
        return !(a || b);
    }
}
