package com.company.corejava.operators;

public class ExampleAssignmentOperators {

    public int assign(int a, int b) {
        a = b;
        return a;
    }

    public int addAndAssign(int a, int b) {
        return a += b; //a = a+b;
    }

    public int subtractAndAssign(int a, int b) {
        return a -= b; //a = a-b;
    }

    public int multipleAndAssign(int a, int b) {
        return a *= b; //a = a-b;
    }

    public int divideAndAssign(int a, int b) {
        return a /= b; //a = a/b;
    }

    public int modulusAndAssign(int a, int b) {
        return a %= b; //a = a%b;
    }

    public int leftShiftAndAssign(int a, int b) {
        return a <<= b; //a = a<<b;
    }

    public int rightShiftAndAssign(int a, int b) {
        return a >>= b; //a = a>>b;
    }

    public int bitWiseAndAssign(int a, int b) {
        return a &= b; //a = a&b;
    }

    public int BITwISEoRAndAssign(int a, int b) {
        return a |= b; //a = a|b;
    }
}
