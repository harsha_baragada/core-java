package com.company.corejava.operators;

public class ArithmeticOperators {

    public int add(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }

    public int multiplication(int a, int b) {
        return a * b;
    }

    public int division(int a, int b) {
        return a / b;
    }

    public int getRemainder(int a, int b) {
        return a % b;
    }

    public int getIncrementedValue(int a) {
        return ++a;
    }

    public int getDecrementedValue(int a) {
        return --a;
    }
}
