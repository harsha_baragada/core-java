package com.company.corejava.wrapperClass;

public class ExampleNumbers {

    int i = 200;
    Integer i1 = 100;


    float weight = 25.5f;
    Float weight1 = 255.5f;


    double salary = 25336.45;
    Double salary1 = 23366.44;

    byte aByte = 121;
    Byte aByte1 = 127;

    short aShort = 1253;
    Short aShort1 = 2366;

    long aLong = 63564666564654654L;
    Long aLong1 = 5768578868586786l;

    int y;

    String phoneNumber = "9999999999";

    public void printX() {

        System.out.println( aLong1.compareTo(aLong));
        System.out.println( i1.equals(i));
        System.out.println(  aLong1.toString());
        System.out.println( Long.parseLong(phoneNumber));

        System.out.println( Math.ceil(salary1));
        System.out.println( Math.floor(salary1));

    }
}
