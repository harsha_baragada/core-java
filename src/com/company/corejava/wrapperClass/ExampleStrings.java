package com.company.corejava.wrapperClass;

public class ExampleStrings {

    String createdString = new String("Hello world");
    char[] helloArray =  {'A','P','P','L','E'};
    String createdString1 = new String(helloArray);
    String fruit = "Banana";

    public void printStrings(){
        System.out.println(createdString);
        System.out.println(helloArray);
        System.out.println(createdString1);
        System.out.println(fruit);

        System.out.println(fruit.length());
        System.out.println(fruit.concat(createdString));
        System.out.println(fruit+" "+createdString);
        System.out.println(fruit.charAt(4));
        System.out.println(fruit.contentEquals("Apple"));

    }


    public void ExampleStringBuilderAndStringBuffer(){
        StringBuffer stringBuffer = new StringBuffer("This is a ");
        stringBuffer.append("String Buffer");
        System.out.println(stringBuffer);
        System.out.println(stringBuffer.reverse());
        System.out.println(stringBuffer.delete(7,11));
        System.out.println(stringBuffer.reverse());
        System.out.println(stringBuffer.insert(3,"inserted text"));

        StringBuilder stringBuilder = new StringBuilder("This is a ");
        stringBuilder.append("String Builder");
        System.out.println(stringBuilder);

    }


}
