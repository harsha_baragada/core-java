package com.company.corejava.wrapperClass;

public class ExampleCharacter {
    char ch = 'k';
    char uniChar = '\u039A';
    char[] characters = {'A', 'P', 'P', 'L', 'E'};
    Character character  = ch;

    public  void printCharacter(){

        //escape sequence
        // Tab
        System.out.println("Tab \t after tab");

        //backspace
        System.out.println("Backspace \b");

        //new line
        System.out.println("newline \n after new line");

        System.out.println("let's say \" Hello \" ");

        System.out.println(characters.toString());
        System.out.println(character.toUpperCase(ch));
        System.out.println(uniChar);
    }
}
