package com.company.corejava;

public class ExampleLoops {

    int a =10;

    public void printFruits(String[] fruits){
        for (int i = 0; i<fruits.length; i++){
            System.out.println(fruits[i]);
        }

        System.out.println("starting of advanced for loop");
        for (String fruit: fruits
             ) {
            System.out.println(fruit);
        }

    }


    public void exampleWhile(){

        while(a<30){
            System.out.println(a);
            a++;
        }
        System.out.println(a);
    }

    public void exampleDoWhileLoop(){
        do{
            System.out.println(a);
            a++;
        }
        while (a<20);
    }

}
