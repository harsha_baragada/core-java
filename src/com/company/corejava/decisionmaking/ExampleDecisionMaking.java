package com.company.corejava.decisionmaking;

public class ExampleDecisionMaking {

    int x = -30;

    public void exampleIfStatement() {
        if (x < 20) {
            System.out.println(x);
            System.out.println("inside if statement");

        } else {
            System.out.println("inside else statement");

        }


    }

    public void exampleNestedIfElseStatement() {
        if (x > 0) {
            System.out.println(x + "  is greater than 0");
            if (x % 10 == 0) {
                System.out.println(x + " is divided by 10");
                if (x % 2 == 0) {
                    System.out.println(x + "is divided by 2");
                } else {
                    System.out.println(x + " is not divided by 2");
                }
                if (x % 3 == 0) {
                    System.out.println(x + " is divided by 3");
                } else {
                    System.out.println(x + "is not divided by 3");

                }
            } else {
                System.out.println(x + " is not divided by 10");
            }
        } else {
            System.out.println(x + " is less than 0");
        }
    }

    public void ExampleSwitchCase() {

        char grade = 'Z';

        switch (grade) {

            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very Good");
            case 'C':
                System.out.println("Good");
                break;
            case 'D':
                System.out.println("Medium");
                break;

            case 'E':
                System.out.println("Just Passed");
                break;

            case 'F':
                System.out.println("Failed");
                break;
            default:
                System.out.println("invalid grade");

        }

    }

    public void exampleTerenaryOperator() {
        String city = "Newyork";

        boolean code = (city == "Newyork") ? true : false;
        System.out.println(code);
    }
}
