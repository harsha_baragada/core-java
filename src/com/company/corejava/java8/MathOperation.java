package com.company.corejava.java8;

@FunctionalInterface
public interface MathOperation {

    void operation();
}
