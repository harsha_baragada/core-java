package com.company.corejava.java8;

import java.util.ArrayList;
import java.util.List;

public class ExampleLambdaExpressions  implements MathOperation{


    //parameter -> expression body

    List<User> users = new ArrayList<>();

    public void operateUser() {
        User user = new User();
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        user.setEmail("user@user.com");
        user3.setEmail("user@user.com");
        user2.setEmail("user@user.com");
        user1.setEmail("user1@user.com");

        user.setId(1001);
        user1.setId(1002);
        user2.setId(1003);
        user3.setId(1004);

        users.add(user);
        users.add(user1);
        users.add(user2);
        users.add(user3);

        users.forEach(result -> System.out.println(result));

        //:: represents the method reference
        System.out.println("****************************************************");
        users.forEach(System.out::println);
        


    }

    public String sayHello() {
     return    "Hello world..!";
    }


    public void operation() {
        System.out.println(2);
    }
}

class User {

    private int id;
    private String name;
    private String email;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
