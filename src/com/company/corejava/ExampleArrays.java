package com.company.corejava;

import java.util.Arrays;

public class ExampleArrays {

    int[] integers; // preferred way
    int integers1[];
    int x;

    String[] fruits = new String[x];
    String[] employees = {"Bob", "Eva", "peter", "Max", "Jane"};

    String[] users = {"Bob", "Eva", "peter", "Max", "Jae"};


    public void printAllEmployees() {

        for (String employee : employees
        ) {
            System.out.println(employee);
        }

        for (int i = 0; i < employees.length; i++) {

            System.out.println("index of " + employees[i] + " is " + i);
        }
        extensionOfName(employees);

    }

    public void extensionOfName(String[] employees) {

        for (String employee :
                employees) {
            System.out.println(employee + ".a");
        }
    }


    public int[] generateNumberIds() {


        int[] ids = {0, 1, 2, 3, 5, 45, 6, 23, 12, 63, 5, 45, };
        return ids;
    }

    public boolean userToEmployee() {
        return Arrays.equals(employees, users);
    }

    public int searchEmployee() {
        int[] ids = {0, 1, 2, 3, 5, 45, 6, 23, 12, 63, 5, 45, };

        Arrays.sort(ids);
        return Arrays.binarySearch(ids, 45);
    }
}
