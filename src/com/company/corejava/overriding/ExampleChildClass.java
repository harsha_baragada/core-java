package com.company.corejava.overriding;

public class ExampleChildClass extends ExampleParentClass{


    @Override
    public void printx(int x) {
        super.printx(x);
        System.out.println("This method will print the value of x in child class");
    }
}
