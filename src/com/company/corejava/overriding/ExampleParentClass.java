package com.company.corejava.overriding;

public class ExampleParentClass {

    public void printx(int x) {
        System.out.println("This method will print the value of x in parent class");
    }

    public void printY() {
        System.out.println("This method will print the value of y in parent class");
    }
}
