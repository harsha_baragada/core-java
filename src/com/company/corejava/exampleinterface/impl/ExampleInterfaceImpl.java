package com.company.corejava.exampleinterface.impl;

import com.company.corejava.exampleinterface.ExampleInterface;

public class ExampleInterfaceImpl extends ExampleAbstractImpl {

    @Override
    public String whoAmI() {
        return "I am an interface :)";
    }
}
