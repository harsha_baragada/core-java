package com.company.corejava.datastructures;

import java.util.Stack;

public class ExampleStack {

    Stack stack = new Stack<String>();

    public Stack getStack() {
        stack.push("Apple");
        stack.push("Mango");
        stack.push("Water melon");
        stack.push("Papaya");
        stack.push("Pears");
        stack.push("Cherry");
        stack.push("Strawberry");
        stack.push("Banana");
        return stack;
    }


}
