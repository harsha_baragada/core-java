package com.company.corejava.datastructures;

import java.util.Hashtable;

public class ExampleHashTable {

    Hashtable hashtable = new Hashtable();

    public Hashtable getHashTable() {

        hashtable.put(1, "Zara");
        hashtable.put(2, "Mike");
        hashtable.put(3, "Nina");
        hashtable.put(4, "Smith");
        hashtable.put(5, "Smith");

        return hashtable;
    }
}
