package com.company.corejava.datastructures;

import java.util.Enumeration;
import java.util.Vector;

public class ExampleEnumeration {

    Enumeration students;
    Vector studentNames = new Vector();

    public void printAllStudentNames() {
        studentNames.add("Peter");
        studentNames.add("Bob");
        studentNames.add("Eva");
        studentNames.add("Mark");
        studentNames.add("Emile");
        studentNames.add("David");
        studentNames.add("Anna");
        studentNames.add("Jane");
        studentNames.add("Mike");

        students = studentNames.elements();
        while (students.hasMoreElements()) {
            System.out.println(students.nextElement());
        }
    }
}
