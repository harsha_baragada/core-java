package com.company.corejava.datastructures;

import java.util.BitSet;

public class ExampleBitSet {

    BitSet bitSet1 = new BitSet(16);
    BitSet bitSet2 = new BitSet(16);

    public void printBitSetExample() {

        for (int i = 0; i < 16; i++) {
            if (i % 2 == 0) bitSet1.set(i);
            if (i % 5 != 0) bitSet2.set(i);
        }
        System.out.println("Initial pattern in bitset1 : ");
        System.out.println(bitSet1);
        System.out.println("Initial pattern in bitset2 : ");
        System.out.println(bitSet2);

        //AND
        bitSet2.and(bitSet1);
        System.out.println("bitSet2 and bit");
        System.out.println(bitSet2);

        bitSet2.or(bitSet1);
        System.out.println("Or bitset1: ");
        System.out.println(bitSet2);

    }
}
