package com.company.corejava;

public class ExampleDataTypes {
    private byte a;
    private short b;
    private int c;
    private long d;
    private float f;
    private double e;
    private char g;
    private boolean h;

    public ExampleDataTypes() {
        System.out.println("Default Constructor for the class ExampleDataTypes is invoked");
    }

    public ExampleDataTypes(int c, long d, char g) {
        this.c = c;
        this.d = d;
        this.g = g;
    }

    public void printDataTypeDefaultValues() {
        System.out.println("printing default values of data types");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(g);
        System.out.println(h);
        a = 127;
        b = 12336;
        c = 10000000;
        d = 32656985566655555L;
        f = 102.0F;
        e = 233.23D;
        g = 'A';
        h = true;
    }

    public void printDataTypes() {
        System.out.println("values assigned");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(g);
        System.out.println(h);
    }
}
