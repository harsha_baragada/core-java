package com.company.corejava.polymorphism;

public class Deer extends Animal implements Vegetarian {

    public void className() {
        System.out.println("This is Deer class");
    }

    @Override
    public void eatGreen() {
        System.out.println("Eat green veg");
    }
}
