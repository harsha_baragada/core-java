package com.company.corejava.polymorphism;

public interface Vegetarian {
    void eatGreen();
}
