package com.company.corejava.generics;

public class ExampleGenerics {


    public static <E> void printFirstElementOfArray(E[] elementsArray){
        System.out.println(elementsArray[0]);
    }
}
