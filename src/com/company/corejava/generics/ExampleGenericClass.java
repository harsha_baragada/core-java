package com.company.corejava.generics;

public class ExampleGenericClass<T> {

    private T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public ExampleGenericClass(T t) {
        this.t = t;
    }

    public ExampleGenericClass() {
    }
}
