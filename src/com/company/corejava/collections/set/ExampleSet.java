package com.company.corejava.collections.set;

import java.util.*;

public class ExampleSet {

    Set<String> students = new HashSet<>();
    AbstractSet<Integer> ids = new HashSet<>();

    public void printStudents() {


        students.add("Peter");
        students.add("Bob");
        students.add("Parker");
        students.add("Mark");
        students.add("Jane");
        students.add("Eva");
        students.add("Emile");
        students.add("Peter");
        System.out.println(students);
    }

    public LinkedHashSet<Integer> exampleLinkedHashSet() {

        LinkedHashSet<Integer> integers = new LinkedHashSet<>();
        integers.add(1001);
        integers.add(1012);
        integers.add(1010);
        integers.add(1023);
        integers.add(1042);
        integers.add(1003);
        integers.add(1002);
        integers.add(1010);

        return integers;
    }

    public SortedSet<Integer> integerSortedSet() {

        Collection<Integer> integers = new LinkedList<>();
        integers.add(1001);
        integers.add(1012);
        integers.add(1010);
        integers.add(1023);
        integers.add(1042);
        integers.add(1003);
        integers.add(1002);
        integers.add(1010);
        SortedSet<Integer> ids = new TreeSet<>(integers);

        return ids;

    }
}
