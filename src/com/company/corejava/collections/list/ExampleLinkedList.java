package com.company.corejava.collections.list;

import java.util.LinkedList;
import java.util.List;

public class ExampleLinkedList {

    List<Integer> list = new LinkedList<>();

    public void printLinkedList() {

        list.add(4);
        list.add(8);
        list.add(1);
        list.add(3);
        list.add(86);
        list.add(4);
        list.add(54);
        list.add(46);
        list.add(84);
        System.out.println(list.get(0));
    }
}
