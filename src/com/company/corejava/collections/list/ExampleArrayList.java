package com.company.corejava.collections.list;

import java.util.*;

public class ExampleArrayList {

    ArrayList<String> strings = new ArrayList<>();
    List<String> stringList = new ArrayList<>(); // The most common way to create array list
    Collection<String> stringCollection = new ArrayList<>();
    AbstractList<String> stringAbstractList = new ArrayList<>();


    public void printCities() {
        stringCollection.add("Newyork");
        stringCollection.add("Dallas");
        stringCollection.add("Texas");
        stringCollection.add("Michigan");
        stringList.addAll(stringCollection);

        stringList.add(0, "Bombay");
        stringList.add("Banglore");
        System.out.println(stringList.size());
        System.out.println(stringList);
        System.out.println(stringList.get(4));
    }


}
