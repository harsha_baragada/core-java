package com.company.corejava.collections.map;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExampleMap {

    Map<Integer, String> integerStringMap = new LinkedHashMap<>();

    public void mapOperartions() {
        integerStringMap.put(3, "Mane");
        integerStringMap.put(1, "Zara");
        integerStringMap.put(2, "Kyle");
        integerStringMap.put(5, "Zara");
        integerStringMap.put(1, "Pinky");
        integerStringMap.put(4, "Arva");

        System.out.println(integerStringMap.size());
        System.out.println(integerStringMap);
        System.out.println(integerStringMap.containsKey(1));
        System.out.println(integerStringMap.containsKey(6));
        System.out.println(integerStringMap.containsValue("Zara"));
        System.out.println(integerStringMap.containsValue("ZARA"));
        System.out.println(integerStringMap.get(4));
        System.out.println(integerStringMap.keySet());

    }

    public Map<Integer, String> getMap() {
        return integerStringMap;
    }
}
