package com.company.corejava.collections.map;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class ExampleSortedMap {


    public void printSortedMap() {
        ExampleMap exampleMap = new ExampleMap();
        exampleMap.mapOperartions();
        Map<Integer, String> returnedMap = exampleMap.getMap();
        SortedMap<Integer, String> integerStringSortedMap = new TreeMap<>(returnedMap);
        System.out.println(integerStringSortedMap);

    }
}
