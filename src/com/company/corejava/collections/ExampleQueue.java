package com.company.corejava.collections;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {

    Queue<String> iphoneQueue = new PriorityQueue<>();

    public void printExampleQueue() {


        iphoneQueue.add("Peter");
        iphoneQueue.add("Bob");
        iphoneQueue.add("Parker");
        iphoneQueue.add("Mark");
        iphoneQueue.add("Jane");
        iphoneQueue.add("Eva");
        iphoneQueue.add("Emile");
        iphoneQueue.add("Peter");

        System.out.println(iphoneQueue);
        System.out.println(iphoneQueue.peek());
        System.out.println(iphoneQueue + " after peek");
        System.out.println(iphoneQueue.remove());
        System.out.println(iphoneQueue + " after remove");
    }
}
