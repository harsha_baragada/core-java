package com.company.corejava.collections;

import java.util.*;

public class ExampleCollections {
    Collection<Integer> ints = new ArrayList<>();

    public Collection<Integer> getCollections() {

        for (int i = 0; i < 100000000; i++) {
            ints.add(i);
        }
        System.out.println("creating a collection has been done");
        return ints;
    }

    public void printCalledCollectionMethods() {
        System.out.println("The size of the collection is " + ints.size());

        ints.add(133);
        /*Iterator itr = ints.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }*/
        System.out.println("Is the collection empty " + ints.isEmpty());
        System.out.println("is 49 present in the collection " + ints.contains(49));
        Collection<Integer> checkInts = new ArrayList<>();
        for (int i = 9999; i < 99999; i++) {
            checkInts.add(i);
        }
        Date startTime = new Date();
        System.out.println(startTime);
        System.out.println("contains all method " + ints.containsAll(checkInts));
        Date endTime = new Date();
        System.out.println(endTime);
    }
}
