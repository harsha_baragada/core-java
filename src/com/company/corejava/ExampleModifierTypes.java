package com.company.corejava;

public  abstract class ExampleModifierTypes {

    // Access Modifiers
    char ch = 'a'; // default access modifier which will be visible through out the package
    public static int max = 100; // this will be visible any where in the project
    protected int min = 0; //  protected will be visible through out the package along with all subclasses
    private String firstName = "Peter"; // This will be visible only with in the class

    //non access modifiers\
    //static
    static int x = 10;

    public static int add(int a, int b) {
        return a + b;
    }

    // final
    // this applies for class, methods and variables

    public final int y = 100;
    public final  int addTwo(final int a,final int b) {
        return a + b;
    }

    public synchronized  void print(){
        System.out.println("Printed");
    }

    //abstract
    public abstract String sayHelloWorld();

}
