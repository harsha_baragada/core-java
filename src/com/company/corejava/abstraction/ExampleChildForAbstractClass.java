package com.company.corejava.abstraction;

public class ExampleChildForAbstractClass extends ExampleAbstractClass{


    @Override
    public String getName() {
        return "This is a child class of abstract class =p";
    }
}
