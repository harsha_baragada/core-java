package com.company;

import com.company.corejava.java8.ExampleLambdaExpressions;
import com.company.corejava.java8.ExampleStreams;
import com.company.corejava.java8.MathOperation;
import com.company.corejava.multithreading.RunnableImpl;

import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) throws Exception {

        ExampleStreams streams = new ExampleStreams();
        streams.exampleStreams();

    }


}



